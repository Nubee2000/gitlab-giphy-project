[Back to Requirement 3](./REQUIREMENTS.md#3-search-for-gifs)

# Project Giphy - Requirements 3 Implementation Guide

## 3. Search for GIFs

Given that a user is using the comment editor, <br>
And the user clicked the **Find and insert GIF** button, <br>
And the **Find and insert GIF** modal opened, <br>
When the user enters text in the modal's search bar, <br>
Then the list of GIFs should be refreshed from Giphy using the search value.

### 3.1. Form Input

You can use the v-model directive to create two-way data bindings on form input, textarea, and select elements. It automatically picks the correct way to update the element based on the input type. Although a bit magical, v-model is essentially syntax sugar for updating data on user input events, plus special care for some edge cases.


```html
<script>
import { GlModal, GlSearchBoxByType } from '@gitlab/ui';

export default {
   components: {
      GlModal, 
      GlSearchBoxByType,
   },
   data() {
    return {
       modalVisible: false,
      searchTerm: '',
    };
  },
}
</script>
<template>
   <gl-modal v-model="modalVisible">
      <gl-search-box-by-type v-model="searchTerm" />
      Content...
   </gl-modal>
</template>
```

> **Hint**

1. Consider using `trim` on the `v-model` [input for better input accuracy](https://v2.vuejs.org/v2/guide/forms.html#trim).

### 3.3. Watch the updated value

We need to make sure we can access the new value in `searchTerm` once the user has finished typing!

```html
<script>
export default {
   watch: {
    searchTerm() {
      // Do something with the new value
    },
  },
}
</script>
```

### 3.3. Building the query URL

Once we have access to the returned data from the searchbox, we want to build the new query in the same fashion we did in Step 2, only using the new `searchTerm` value.


```html
<script>
export default {
   data() {
    return {
      searchTerm: '',
    };
  },
  methods: {
    async searchGifs() {
      const url = new URL(`${apiUrl}gifs/search?&q=${searchTerm}&limit=100`);

      const response = await window.fetch(url);
      const asJson = await response.json();

      console.log(asJson);
    }
  }
}
</script>
```

### 3.4. Update results

We now need to update the list of our displayed GIFs inside the modal, this can be achieved via updating our reactive `data` property.

### 3.5. Debounce the query

Include a 250ms [debounce](https://css-tricks.com/debouncing-throttling-explained-examples/) between the user input and the network request.

### 3.6. Clear the results

If the search is cleared, fetch the trending GIFs with Giphy's [trending endpoint](https://developers.giphy.com/docs/api/endpoint#trending).

### Other resources

- [VueJS Form Binding](https://v2.vuejs.org/v2/guide/forms.html)
- [VueJS Watch](https://vuejs.org/v2/api/#watch)
- [Learn to use flex layout with the flex froggy game](https://flexboxfroggy.com/)
- [GitLab UI’s infinite scroll component](https://gitlab-org.gitlab.io/gitlab-ui/?path=/docs/base-infinite-scroll--default)
- [GitLab UI’s search by type component](https://gitlab-org.gitlab.io/gitlab-ui/?path=/docs/base-search-box-by-type--default)
