# About `fetch` in JavaScript

JavaScript running on a browser will often need to make HTTP requests to servers
without reloading the page.

Traditionally, this was done with [`XMLHttpRequest`](https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest) (or xhr),
but today there is a much friendlier and widely supported function called [`fetch`](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API).

## Example

```javascript
// Let's pretend that this url returns content that looks like:
// {
//   "content": "Lorem ipsum", 
// }
const url = 'https://example.com/path/to/endpoint';
const responsePromise = fetch(url);

responsePromise
  // - We "unbox" the response using Promise's ".then"
  // - Resposne has a "json" method which deserializes the raw 
  // response body into an actual JavaScript object.
  // - We return the result of ".json" so that the next ".then"
  //   can unbox the result of ".json"
  .then(res => res.json())
  // - `data` will be the unboxed result of "res.json"
  .then(data => {
    document.getElementById('root').textContent = data.content;
  });
```

- `fetch` takes a URL as the first parameter which can be either a
  String or a [`URL` object](https://developer.mozilla.org/en-US/docs/Web/API/URL). `fetch` also takes an **optional** second parameter which includes more options for the request (such as, `headers`).
- `fetch` returns a [`Response` object](https://developer.mozilla.org/en-US/docs/Web/API/Response)
  wrapped in a [`Promise`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise).

## Advanced example

For a more real-life example of `fetch`, see 
this CodeSandbox [`ase-gitlab-projects-explorer`](https://codesandbox.io/s/ase-gitlab-projects-explorer-1szef). This will
query and present real GitLab projects by making requests to 
GitLab's API at `https://gitlab.com/api/v4/projects`
([relevant docs](https://docs.gitlab.com/ee/api/projects.html#list-all-projects)).

- The call to `fetch` lives in the `service.js` module, which 
  exports a `fetchProjects` function. This function takes an 
  argument that will set the Query parameters of the URL. The 
  `page` query parameter enables us to paginate through the results.
- The `App.vue` component makes the call to `fetchProjects`
  whenever the component is intially created, and then everytime
  the "Load More" button is clicked.
- The `App.vue` component separates `projects` and `projectsById`
  because subsequent page requests could contain projects that 
  have already been saved.

### Things to try

- In `service.js`, Change the default `order_by` set in the 
  `DEFAULT_PARAMS` constant. Use the [GitLab API docs](https://docs.gitlab.com/ee/api/projects.html#list-all-projects) to learn
  what values the GitLab API server supports.
- In `service.js`, what happens if you remove the line that 
  starts with `url.search = `?

## Going further

### How to add query string parameters

As seen by the above advanced example, you can use the 
[URL](https://developer.mozilla.org/en-US/docs/Web/API/URL) interface
to easily manipulate the query string parameters of the URL,
you need to make an HTTP request to.

Your will also use the [`URLSearchParams`](https://developer.mozilla.org/en-US/docs/Web/API/URLSearchParams/URLSearchParams)
interface.

```javascript
function fetchStuff(page, orderBy) {
  // 1. Not so good...
  // - What if baseUrl already has query string params?
  // - What if there's extra params we also need to support?
  // - What if page contains unexpected characters like "&"
  fetch(`${baseUrl}?page=${page}&order_by=${orderBy}`)

  // 2. Much better :)
  const url = new URL(baseUrl);
  url.search = new URLSearchParams({ page, order_by: orderBy });
  fetch(url);
}
```
